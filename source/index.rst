.. Raj Ranjan Resume documentation master file, created by
   sphinx-quickstart on Wed Jan 30 14:50:50 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Raj Ranjan Resume
=============================================

.. image:: ./images/youtube-logo.png
   :height: 30px
   :width: 40 px
   :alt: alternate text

.. toctree::
   :maxdepth: 1


   job-experience.rst
   skills.rst
   education.rst
   projects.rst


