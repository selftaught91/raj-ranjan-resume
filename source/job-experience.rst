Job Experience
=================================

Lead Software Engineer Research & Development at Alepo Technologies
#########################################################################

   -  Alepo Technologies
   -  April 2018 : Present 
   -  Mumbai India

Senior Software Engineer Research & Development At Alepo Technologies
############################################################################
   
   -  Alepo Technologies
   -  April 2016 : April 2018 **( 2Years 1Month)**
   -  Mumbai India


Software Engineer Research & Development At Alepo Technologies
########################################################################

   -  Alepo Technologies
   -  May 2015 : March 2016 **( 11 Months )**
   -  Mumbai India


Graduate Engineer Trainee
############################################################

   -  Honda Motorcycle & Scooter India Pvt Ltd
   -  July 2014 : May 2015 **( 11 Months )**
   -  Delhi India


Intership
###########################################################

   -  Mahindra Reva Electric Vechiles Pvt. Ltd.
   -  May 2013 : June 2013 **( 2 Months )**
   -  Bangalore India


Internship
##########################################################

   -  Condign Solutions
   -  May 2012 : July 2012 **( 3 Months )**
   -  Bihar India

     
