Educational Qualification
=====================================


   .. csv-table:: Educational Qualification
      :header: "Year", "Program", "Board/Institute","%/CPI"
      :widths: 20, 40, 20, 20

      "2007","Class X", "CBSE(India)",95.6
      "2009","Class XII","CBSE(India)",78.6
      "2014","B.Tech Mechanical","NIT Silchar",7.6



