Projects
===============================

.. note::
   Here you will find all the projects that I have done



Billing Engine
####################################

   -  2018 : Present
   -  Technologies Used

      -  Python
      -  Mysql
      -  Flask Framework
      -  REST API's

Rating Engine
####################################

   -  2018 : Present
   -  Technologies Used

      -  Python
      -  MongoDB
      -  Flask Framework
      -  REST API's

Partner Management System
#####################################

   -  2018 : Present 
   -  Technologies Used

      -  NodeJs
      -  AngularJs
      -  MongoDB
      -  REST API's


Fulfilment Manager
#####################################

   -  2017 : 2018
   -  Technologies Used   

      -  Python
      -  Nodejs
      -  AngularJs
      -  MongoDB

Rule Promotion Manager   
####################################
   
   -  2016 : 2017
   -  Technologies Used

      -  NodeJs
      -  AngularJs
      -  MongoDB
      -  REST API's

Research Manager   
####################################
   
   -  2016 : 2017
   -  Technologies Used

      -  NodeJs
      -  AngularJs
      -  MongoDB
      -  REST API's

Product Catalog
####################################
   
   -  2015 : 2016
   -  Technologies Used

      -  NodeJs
      -  AngularJs
      -  MongoDB
      -  REST API's



